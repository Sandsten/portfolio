export const BASE00 = '#657b83';
export const BASE01 = '#586e75';
export const BASE02 = '#073642';
export const BASE03 = '#002b36';

export const BASE02_SATURATED = '#002028';

export const BASE0 = '#839496';
export const BASE1 = '#93a1a1';
export const BASE2 = '#eee8d5';
export const BASE3 = '#fdf6e3';

export const BASE2_SATURATED = '#eddeaf';

export const YELLOW = '#b58900';
export const ORANGE = '#cb4b16';
export const RED = '#dc322f';
export const MAGENTA = '#d33682';

export const VIOLET = '#6c71c4';
export const BLUE = '#268bd2';
export const CYAN = '#2aa198';
export const GREEN = '#859900';
